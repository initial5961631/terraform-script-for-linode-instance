# Automated Nginx Deployment on Linode Instances with Terraform Script

This repository hosts Terraform configuration files that automate Linode server provisioning, Nginx deployment, and firewall management.

#### Prerequisites

Before you begin, ensure you have the following prerequisites:

- Linode account
- Terraform installed
- Linode API access token

**Getting Started**

Follow these steps to get started with provisioning the Linode server, deploying the Nginx server, and managing firewall rules:

1. Clone this repository:
   
   ```bash
   git clone https://gitlab.com/initial5961631/terraform-script-for-linode-instance.git
   ```

2. Inside the cloned directory, insert the required variable parameters in `terraform.tfvars`. These parameters are essential to run the `main.tf` configuration. Refer to the comment section in the `main.tf` file for information on these variables.

3. Initialize the Terraform provider by running:
   
   ```bash
   terraform init
   ```

   This command informs Terraform which provider you are using and prepares the environment for deployment.

4. Preview the changes that Terraform will make with:
   
   ```bash
   terraform plan
   ```

   This step ensures that the main configuration is working correctly and shows you what changes will occur during the deployment.

5. Apply the configuration by executing:
   
   ```bash
   terraform apply
   ```

   This command implements the configuration, deploys the Linode and Nginx instances, and enforces the defined firewall rules.

6. [Optional] If you prefer a scripted version, you can explore the alternative branch.

By following these steps, you will provision a Linode VPC instance, deploy an Nginx server, and establish firewall rules. If you encounter any issues or need further assistance, refer to the provided documentation or seek help from the community.

Have a Great Day! 😊
