# In case of cloud provider that are not managed by hasicrop we might need to add this block explaining terraforrm to select the providers source

terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
  }
}

# Defining the variable for the token key which will be used by the provider
variable "token_key" {
  description = "secret"
}

# Provider is basically a VPC of platform on which you want to perform your desired action
provider "linode" {
  token = var.token_key # Based on the provider or cloud platform you are required to provide the token or secret key or both after which you are provided access to manage your service 
}


# Defining the variable for to set the public ssh_key which is generated inorder to access the server after the deployment
variable "root_pass" {
  description = "main-password"
}

# Defining the variable for to set the public ssh_key which is generated inorder to access the server after the deployment
variable "ssh_key" {
  description = "ssh"
}


# This is bascailly a segment where you start to craft your service and available options that are allowed to configure can be found on the terraform website within the linode for others you can go and have a look at yourself
resource "linode_instance" "core_service" {
  label       = "Server_Instance-001"
  type        = "g6-nanode-1"
  region      = "ap-southeast"
  image       = "linode/debian11"
  root_pass = var.root_pass #note :- if you wish you can only use the root_pass section omitting the authorized_key section which will provide you the access to server thorugh the linode portal manually
  authorized_keys = var.ssh_key

  connection {
    type        = "ssh"
    user        = "root"
    private_key =  file("./id_rsa") # providing path to your private key
    host        = linode_instance.core_service.ip_address # using the instance's IP address
  }

  provisioner "remote-exec" {
    #script = "command.sh" # There are different ways in which you can provide the executable command one is with inline where you can give individual instruction on what you want to execute else you can create your own script and execute that in a remote host.
     inline = [
       "echo 'Hello from the remote server'",
       "sudo apt install -y ufw"
       "sudo apt update && sudo apt install -y nginx",
       "sudo systemctl start nginx",
       "sudo ufw allow OpenSSH",
       "sudo ufw allow http",
       "sudo ufw --force enable"
     ]
}
}
